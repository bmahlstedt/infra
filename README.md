Contains the docker composition for the nginx reverse proxy,
currently hosting southbaysupercontest.com and bmahlstedt.com
on a single digitalocean droplet.

You should only need to do the following once:
```bash
docker network create nginx-proxy
docker-compose up -d --build
```

The you can bring up any other containers running services
with `VIRTUAL_HOST`, etc.

If websites behind this proxy report expired certs (like supercontest
when you leave it untouched >6mo between seasons), simply go to
`code/infra` on the Digital Ocean droplet and run:
```bash
docker-compose restart
```
